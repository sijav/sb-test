## installation

You need Git, NodeJs (>= 14.17) and Yarn:

- Install Git (the way you like it)
- Install LTS version of NodeJs from: https://nodejs.org/en/download/
- Install Yarn by running: `npm i -g yarn`
- Clone this repo: `git clone https://gitlab.com/sijav/sb-test.git`
- Install dependencies by running `yarn`


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />

This will run two scripts concurrently:
- dev:react
- dev:electron

### `yarn dev:react`

Starts the react dev server and will watch for files, it also serves the result on http://localhost:4000

### `yarn dev:electron`

Open the electron app in dev mode.

### `yarn format`

Will format all codes with prettify.

### `yarn sort`

Will sort the imports.

### `yarn react:lint`

Will check for errors in react file.

### `yarn electron:lint`

Will check for errors in electron.

## Project Structure

- Html staticfiles are on `public` folder
- Electron files are on `electron` folder with starting point file `main.ts.`
- React.JS files are on `src` folder with starting point file `index.tsx`
- webpack for electron areon the root of the project named `webpack.electron.config.js`
- All the react webpack configs are in `react-scripts` folder which is a copy pasted react-scripts package with a bit of tweek for electron app.
