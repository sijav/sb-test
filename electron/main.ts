import { app, BrowserWindow, ipcMain } from 'electron';
import fs from 'fs';
import path from 'path';
import os from 'os';

let mainWindow: Electron.BrowserWindow | null;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  if (process.env.NODE_ENV === 'development') {
    mainWindow.loadURL(`http://localhost:4000`);
  } else {
    mainWindow.loadFile('index.html');
  }

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  return mainWindow;
}

app.whenReady().then(() => {
  const win = createWindow();

  win.setRepresentedFilename(os.homedir());
  win.setDocumentEdited(true);
});

ipcMain.on('get-dir', (event, arg) => {
  const currentPath = path.join(app.getPath('home'), arg);
  fs.promises
    .readdir(currentPath, { withFileTypes: true })
    .then((dirents) =>
      Promise.all(
        dirents.map((dirent) =>
          app.getFileIcon(path.join(currentPath, dirent.name)).then((icon) => ({
            icon: icon.toPNG().toJSON().data,
            name: dirent.name,
            isDirectory: dirent.isDirectory(),
          })),
        ),
      ),
    )
    .then((list) => {
      event.reply('dir-list', { arg, list });
    })
    .catch((err) => {
      event.reply('dir-list', { arg, err });
    });
});

ipcMain.on('rename', (event, arg) => {
  const oldPath = path.join(app.getPath('home'), arg.dir, arg.oldName);
  const newPath = path.join(app.getPath('home'), arg.dir, arg.newName);
  if (fs.existsSync(oldPath)) {
    fs.promises.rename(oldPath, newPath).then(
      () => {
        event.reply(`rename-${arg.dir}-${arg.oldName}-${arg.newName}`, {
          ...arg,
        });
      },
      (err) => {
        event.reply(`rename-${arg.dir}-${arg.oldName}-${arg.newName}`, {
          ...arg,
          err,
        });
      },
    );
  }
});

ipcMain.on('delete', (event, arg) => {
  const filePath = path.join(app.getPath('home'), arg.dir, arg.name);
  if (fs.existsSync(filePath)) {
    fs.promises.unlink(filePath).then(
      () => {
        event.reply(`delete-${arg.dir}-${arg.name}`, {
          ...arg,
        });
      },
      (err) => {
        event.reply(`delete-${arg.dir}-${arg.name}`, {
          ...arg,
          err,
        });
      },
    );
  }
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// app.allowRendererProcessReuse = true;
