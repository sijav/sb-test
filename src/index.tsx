import { App } from './App';
import ReactDom from 'react-dom';

const mainElement = document.createElement('div');
document.body.appendChild(mainElement);

ReactDom.render(<App />, mainElement);
