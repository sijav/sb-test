export function removeFromImmutableArray<ImmutableArray extends unknown[]>(array: ImmutableArray, predict: (item: ImmutableArray[0]) => boolean): ImmutableArray {
  const foundIndex = array.findIndex(predict);
  if (foundIndex > -1) {
    const newArray: ImmutableArray = [...array] as ImmutableArray;
    newArray.splice(foundIndex, 1);
    return newArray;
  }
  return array;
}
