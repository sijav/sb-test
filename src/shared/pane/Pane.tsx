import { FCProps } from 'src/shared/types/FCProps';
import { MouseEventHandler, useEffect, useRef } from 'react';

interface OwnProps {
  active: boolean;
  onClick: MouseEventHandler<HTMLDivElement>;
}

type Props = FCProps<OwnProps>;
export const Pane = (props: Props) => {
  const { children, onClick, active } = props;
  const paneEl = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const content = document.getElementsByClassName('window-content')[0];
    if (active) {
      setTimeout(() => {
        if (active && paneEl.current && content) {
          content.scrollTo({
            left: paneEl.current.offsetLeft,
            behavior: 'smooth',
          });
        }
      }, 150);
    }
  }, [active]);

  useEffect(() => {}, []);
  return (
    <div className="pane" ref={paneEl} style={{ minWidth: 400 }} onClick={onClick}>
      {children}
    </div>
  );
};
