import { FileItem, FileItemType } from 'src/shared/file-item';
import { useCallback, useEffect, useState } from 'react';

import { FCProps } from 'src/shared/types/FCProps';
import { Pane } from 'src/shared/pane';
import { ipcRenderer } from 'electron';

interface OwnProps {
  dir: string;
  active: boolean;
  setActive: (isActive: boolean, force?: boolean) => void;
}

type Props = FCProps<OwnProps>;
export const FileListPane = (props: Props) => {
  const { dir, active, setActive } = props;

  const [isActiveNext, setIsActiveNext] = useState(false);
  const [activeItem, setActiveItem] = useState<string>();
  const [secondDir, setSecondDir] = useState<string>();
  const [list, setList] = useState<FileItemType[]>([]);

  const handleActiveNext = (shouldActive: boolean, force: boolean = false) => {
    setIsActiveNext(shouldActive);
    if (force) {
      setActive(shouldActive, force);
    }
  };

  const handleSecondDir = useCallback(
    (dir: string) => {
      setSecondDir(dir);
      setIsActiveNext(true);
    },
    [setSecondDir, setIsActiveNext],
  );

  const onFileList = useCallback(
    (
      _: Electron.IpcRendererEvent,
      arg: {
        arg: string;
        err?: NodeJS.ErrnoException;
        list: FileItemType[];
      },
    ) => {
      if (!arg.err && arg.list && arg.arg === dir) {
        setList([
          ...arg.list.filter((file) => file.isDirectory).sort((file1, file2) => (file1.name.toLowerCase() > file2.name.toLowerCase() ? 1 : -1)),
          ...arg.list.filter((file) => !file.isDirectory).sort((file1, file2) => (file1.name.toLowerCase() > file2.name.toLowerCase() ? 1 : -1)),
        ]);
      }
    },
    [dir, setList],
  );

  useEffect(() => {
    setList([]);
    setSecondDir(undefined);
    setActiveItem(undefined);
    ipcRenderer.once('dir-list', onFileList);
    ipcRenderer.send('get-dir', dir);
  }, [dir, setList, setSecondDir, setActiveItem, onFileList]);

  const onDeleteSuccess = useCallback(
    (
      _: Electron.IpcRendererEvent,
      arg: {
        dir: string;
        name: string;
        err?: NodeJS.ErrnoException;
      },
    ) => {
      setList((oldList) => {
        const itemIndex = oldList.findIndex((item) => item.name === arg.name);
        if (itemIndex > -1) {
          const newList = [...oldList];
          if (!arg.err) {
            newList.splice(itemIndex, 1);
            return newList;
          } else {
            newList[itemIndex].disabled = false;
            return newList;
          }
        }
        return oldList;
      });
    },
    [setList],
  );

  const handleDelete = useCallback(
    (name: string) => {
      setList((oldList) => {
        const itemIndex = oldList.findIndex((item) => item.name === name);
        if (itemIndex > -1) {
          const newList = [...oldList];
          newList[itemIndex].disabled = true;
          return newList;
        }
        return oldList;
      });
      ipcRenderer.once(`delete-${dir}-${name}`, onDeleteSuccess);
      ipcRenderer.send('delete', { dir, name });
    },
    [dir, setList, onDeleteSuccess],
  );

  const onRenameSuccess = useCallback(
    (
      _: Electron.IpcRendererEvent,
      arg: {
        dir: string;
        oldName: string;
        newName: string;
        err?: NodeJS.ErrnoException;
      },
    ) => {
      setList((oldList) => {
        const itemIndex = oldList.findIndex((item) => item.name === arg.oldName);
        if (itemIndex > -1) {
          const newList = [...oldList];
          if (!arg.err) {
            newList[itemIndex].name = arg.newName;
            newList[itemIndex].disabled = false;
            return newList;
          } else {
            newList[itemIndex].disabled = false;
            return newList;
          }
        }
        return oldList;
      });
    },
    [setList],
  );

  const handleRename = useCallback(
    (oldName: string, newName: string) => {
      setList((oldList) => {
        const itemIndex = oldList.findIndex((item) => item.name === oldName);
        if (itemIndex > -1) {
          const newList = [...oldList];
          newList[itemIndex].disabled = true;
          return newList;
        }
        return oldList;
      });
      ipcRenderer.once(`rename-${dir}-${oldName}-${newName}`, onRenameSuccess);
      ipcRenderer.send('rename', { dir, oldName, newName });
    },
    [dir, setList, onRenameSuccess],
  );

  const onKeyPressed = useCallback(
    (event: KeyboardEvent) => {
      const key = event.key.toLowerCase();
      if (active && !isActiveNext) {
        if (key === 'arrowdown') {
          event.preventDefault();
          setActiveItem((oldActiveItem) => {
            const thisActiveIndex = list.findIndex((item) => item.name === oldActiveItem);
            if (thisActiveIndex > -1) {
              return list[thisActiveIndex + 1]?.name ?? list[list.length - 1].name;
            }
            return list[0]?.name;
          });
        }
        if (key === 'arrowup') {
          event.preventDefault();
          setActiveItem((oldActiveItem) => {
            const thisActiveIndex = list.findIndex((item) => item.name === oldActiveItem);
            if (thisActiveIndex > -1) {
              return list[thisActiveIndex - 1]?.name ?? list[0].name;
            }
            return list[list.length - 1]?.name;
          });
        }
        if (key === 'arrowleft') {
          event.preventDefault();
          setActive(false);
        }
        if (key === 'arrowright') {
          if (secondDir === activeItem) {
            event.preventDefault();
            setIsActiveNext(true);
          } else if (activeItem && list.find((item) => item.name === activeItem && item.isDirectory)) {
            event.preventDefault();
            handleSecondDir(activeItem);
          }
        }
        if (key === 'delete') {
          event.preventDefault();
          if (activeItem) {
            handleDelete(activeItem);
          }
        }
      }
    },
    [active, handleDelete, isActiveNext, setActiveItem, list, secondDir, activeItem, setActive, setIsActiveNext, handleSecondDir],
  );

  useEffect(() => {
    if (active && !isActiveNext) {
      window.addEventListener('keyup', onKeyPressed);
    }
    if (!active) {
      setIsActiveNext(false);
    }
    return () => {
      try {
        window.removeEventListener('keyup', onKeyPressed);
      } catch (_) {}
    };
  }, [active, isActiveNext, onKeyPressed]);

  return (
    <>
      <Pane
        active={active && !isActiveNext}
        onClick={() => {
          setActive(true, true);
          setIsActiveNext(false);
        }}
      >
        <nav className="nav-group">
          <h5 className="nav-group-title">{dir}</h5>
          {list.map((file, i) => (
            <FileItem
              {...file}
              onClick={setActiveItem}
              onDoubleClick={handleSecondDir}
              onRename={handleRename}
              key={i}
              active={active && !isActiveNext && file.name === activeItem}
            />
          ))}
        </nav>
      </Pane>
      {secondDir ? <FileListPane dir={(dir === '/' ? '' : dir) + '/' + secondDir} active={isActiveNext} setActive={handleActiveNext} /> : null}
    </>
  );
};
