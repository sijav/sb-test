import { createContext, useCallback, useContext, useState } from 'react';

import { FCProps } from 'src/shared/types/FCProps';
import { removeFromImmutableArray } from 'src/shared/utils/removeFromImmutable';

export interface ActiveFileType {
  dir: string;
  file: string;
}

export interface FileContextType {
  activeDir: string;
  setActiveDir: (dir: string) => void;
  activeFiles?: ActiveFileType[];
  addActiveFile: (activeFile: ActiveFileType) => void;
  removeActiveFile: (activeFile: ActiveFileType) => void;
  disabledFiles?: ActiveFileType[];
  addDisabledFile: (activeFile: ActiveFileType) => void;
  removeDisabledFile: (activeFile: ActiveFileType) => void;
}
export const FileContext = createContext<FileContextType | null>(null);

export function useFileContext(): FileContextType {
  const context = useContext(FileContext);
  if (context === null) {
    throw new Error('useFileContext must be used inside the <FileProvider />');
  }
  return context;
}

interface OwnProps {}

type Props = FCProps<OwnProps>;

export function FileProvider(props: Props) {
  const [activeDir, setActiveDir] = useState('/');
  const [activeFiles, setActiveFiles] = useState<ActiveFileType[]>([]);
  const [disabledFiles, setDisabledFiles] = useState<ActiveFileType[]>([]);

  const handleAddActiveFile = useCallback(
    (activeFile: ActiveFileType) => {
      setActiveFiles((oldActiveFiles) =>
        oldActiveFiles.find((file) => file.dir === activeFile.dir && file.file === activeFile.file) ? oldActiveFiles : [...oldActiveFiles, activeFile],
      );
    },
    [setActiveFiles],
  );

  const handleRemoveActiveFile = useCallback(
    (activeFile: ActiveFileType) => {
      setActiveFiles((oldActiveFiles) => removeFromImmutableArray(oldActiveFiles, (file) => file.dir === activeFile.dir && file.file === activeFile.file));
    },
    [setActiveFiles],
  );

  const handleAddDisabledFile = useCallback(
    (disabledFile: ActiveFileType) => {
      setDisabledFiles((oldDisabledFiles) =>
        oldDisabledFiles.find((file) => file.dir === disabledFile.dir && file.file === disabledFile.file) ? oldDisabledFiles : [...oldDisabledFiles, disabledFile],
      );
    },
    [setDisabledFiles],
  );

  const handleRemoveDisabledFile = useCallback(
    (disabledFile: ActiveFileType) => {
      setDisabledFiles((oldDisabledFiles) => removeFromImmutableArray(oldDisabledFiles, (file) => file.dir === disabledFile.dir && file.file === disabledFile.file));
    },
    [setDisabledFiles],
  );

  return (
    <FileContext.Provider
      value={{
        activeDir,
        setActiveDir,
        activeFiles,
        addActiveFile: handleAddActiveFile,
        removeActiveFile: handleRemoveActiveFile,
        disabledFiles: disabledFiles,
        addDisabledFile: handleAddDisabledFile,
        removeDisabledFile: handleRemoveDisabledFile,
      }}
    >
      {props.children}
    </FileContext.Provider>
  );
}
