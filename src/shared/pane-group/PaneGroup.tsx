import { FCProps } from 'src/shared/types/FCProps';

interface OwnProps {}

type Props = FCProps<OwnProps>;
export const PaneGroup = (props: Props) => {
  const { children } = props;
  return <div className="pane-group">{children}</div>;
};
