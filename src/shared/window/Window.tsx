import { FCProps } from 'src/shared/types/FCProps';

interface OwnProps {}

type Props = FCProps<OwnProps>;
export const Window = (props: Props) => {
  const { children } = props;
  return (
    <div className="window">
      <div className="window-content">{children}</div>
    </div>
  );
};
