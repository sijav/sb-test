import { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';

import { FCProps } from 'src/shared/types/FCProps';

export type FileItemType = {
  icon: number[];
  name: string;
  dir: string;
  isDirectory: boolean;
  disabled: boolean;
};

export interface OwnProps extends FileItemType {
  onClick: (name: string) => void;
  onDoubleClick: (name: string) => void;
  onRename: (oldName: string, newName: string) => void;
  active: boolean;
}

type Props = FCProps<OwnProps>;
export const FileItem = (props: Props) => {
  const { icon, name, isDirectory, onClick, onDoubleClick, onRename, active } = props;

  const [isRenaming, setIsRenaming] = useState(false);
  const secondName = useRef<string>();

  const handleClick = useCallback(
    (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
      e.preventDefault();
      if (active) {
        if (isDirectory) {
          onDoubleClick(name);
        } else {
          setIsRenaming(true);
        }
      } else {
        onClick(name);
      }
    },
    [active, onClick, onDoubleClick, isDirectory, name],
  );

  const handleRename = useCallback(() => {
    if (secondName.current && secondName.current !== name) {
      onRename(name, secondName.current);
    }
    setIsRenaming(false);
  }, [setIsRenaming, onRename, name]);

  const handleChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    secondName.current = e.target.value;
  }, []);

  const onKeyPressed = useCallback(
    (event: KeyboardEvent) => {
      if (active) {
        const key = event.key.toLowerCase();
        if (isRenaming) {
          if (key === 'arrowup' || key === 'arrowdown' || key === 'arrowleft' || key === 'arrowright' || key === 'enter' || key === 'tab' || key === 'escape') {
            event.preventDefault();
            handleRename();
          }
        } else if (key === 'f2') {
          event.preventDefault();
          setIsRenaming(true);
        }
      }
    },
    [isRenaming, setIsRenaming, handleRename, active],
  );

  useEffect(() => {
    if (active) {
      window.addEventListener('keyup', onKeyPressed);
    }
    return () => {
      try {
        window.removeEventListener('keyup', onKeyPressed);
      } catch (_) {}
    };
  }, [active, onKeyPressed]);

  const iconBlob = new Blob([new Uint8Array(icon)]);
  const iconUrl = URL.createObjectURL(iconBlob);

  return (
    <a className={`nav-group-item${active ? ' active' : ''}`} onClick={handleClick}>
      {isDirectory ? (
        <span className="icon icon-folder" />
      ) : (
        <img
          className="icon"
          style={{
            width: 30,
            height: 30,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'contain',
            backgroundPosition: '50%',
            backgroundImage: `url('${iconUrl}')`,
            border: 'none',
            boxShadow: 'none',
          }}
        />
      )}
      {!isRenaming ? (
        name
      ) : (
        <input
          defaultValue={name}
          autoFocus
          onChange={handleChange}
          onBlur={handleRename}
          style={{
            padding: 0,
            margin: 0,
            background: 'transparent',
            boxShadow: 'none',
            border: 'none',
          }}
        />
      )}
    </a>
  );
};
