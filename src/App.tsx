import 'src/assets/css/photon.css';

import { FileListPane } from 'src/shared/file-list-pane';
import { Window } from 'src/shared/window';
export const App = () => {
  return (
    <Window>
      <FileListPane dir="/" active setActive={() => {}} />
    </Window>
  );
};
